<?php
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::uses('UploadAppModel', 'Upload.Model');
class Upload extends UploadAppModel {

	public $uploadOptions = null;


	
	public $validate = array(
		'mime' => array(
			'mimetype' => array(
				'rule' => array('mimetype', array(
					'image/png',
					'image/jpeg',
					'image/gif',

					'video/mp4',
					'video/webm',
					'video/ogv',

					'audio/mp4',
				)),
				'message' => 'Invalid mime type',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'path' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

		'url' => array(
			'url' => array(
				'rule' => array('url'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);



	public function beforeSave($options = array()) {
		$this->uploadOptions = Configure::read('uploadOptions');
		
		if(isset($this->data[$this->alias]['error']) && $this->data[$this->alias]['error'] === 0) {
			$uploadPath = $this->uploadOptions['uploadPath'];

			$dir = new Folder(WWW_ROOT . $uploadPath, true, 0777);
			$file = $this->data[$this->alias]['name'];
			$tmp = $this->data[$this->alias]['tmp_name'];
			$file = pathinfo($file);
			$file['dirname'] = '';
			$upload = $this->__duplicateNames($this->uploadOptions['uploadPath'] . $file['dirname'].$file['basename'], $count = 0);
			$type = explode(DS, $this->data[$this->alias]['type']);

			$file = array(
				'dirname' => '',
				'tmp' => $tmp,
				'upload' => $upload,
				'url' => $upload,
				'size' => $this->data[$this->alias]['size'],
				'mime' => $this->data[$this->alias]['type'],
				'type' => $type['0'],
			);

			

		


			$file['dirname']  = $file['dirname'] . $file['upload'];
			$file['url']  = Router::url('/' . $file['dirname'], true);
			


			$this->log($file, 'customlog');


			
			if(move_uploaded_file($file['tmp'], WWW_ROOT . $file['dirname'])) {
				chmod(WWW_ROOT.$file['dirname'],0777);
				$file['path'] = $file['dirname'];
				$file['url'] = Router::url('/'.$file['dirname'], true);
				$file['url'] = str_replace('\\', '/', $file['url']);
				$file['url'] = str_replace('//', '/', $file['url']);
				return $this->data['Upload'] = $file;
			}

			return false;

		}

		return false;
	}

	protected function __duplicateNames($checkfile, $count = 0) {
		$file = pathinfo($checkfile);
		if($count > 0) {
			$checkfile = $file['dirname'] . DS .   Inflector::slug($file['filename']) . '-' . $count . '.' . $file['extension'];
		}

		if(!file_exists(WWW_ROOT.$checkfile)) {
			$checkfile = $file['dirname'] . DS .   Inflector::slug($file['filename']) . '.' . $file['extension'];
			return $checkfile;
		} else {
			$count++;
			$checkfile = $file['dirname'] . DS .   Inflector::slug($file['filename']) . '.' . $file['extension'];
			return $this->__duplicateNames($checkfile, $count);
		}
	}

	public function beforeDelete($cascade = true){
		$file = $this->field('url');
		$info = pathinfo($file);
		foreach(glob(WWW_ROOT . $info['dirname'] . DS . $info['filename'] . '_*x*' . '.' . $info['extension']) as $v){
			unlink($v);
		}
		foreach(glob(WWW_ROOT . $info['dirname'] . DS . $info['filename'] . '.' . $info['extension']) as $v){
			unlink($v);
		}
		return true;
	}
}