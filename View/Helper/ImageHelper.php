<?php
App::uses('AppHelper', 'View/Helper');
App::uses('HtmlHelper', 'View/Helper'); // Should fix Not Found Exception//
App::uses('Set', 'Utility');
App::uses('Image', 'Upload.Lib');
class ImageHelper extends AppHelper {

	public $helpers = array('Html');


	public function imageResize($image, $width, $height) {

		$newIMG = pathinfo($image);

		$newImage = $newIMG['dirname'] . DS . $newIMG['filename'] . '_' . $width . 'x' . $height . '.' . $newIMG['extension'];
		
		if(!file_exists(WWW_ROOT . $newImage)) {
			$IMAGELIB = new Image();
			$IMAGELIB->prepare(WWW_ROOT . $image);
			$IMAGELIB->resize($width, $height);
			$IMAGELIB->save(WWW_ROOT . $newImage);

			chmod(WWW_ROOT.$newImage,0777);
		}
	
		$newImage = str_replace('\\', '/', $newImage);
		$newImage = str_replace('//', '/', $newImage);
		return $this->Html->image(Router::url($newImage, true));
	}
}

?>