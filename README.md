#Delete Syestem is broken!

#Introduction:
This plugin is based on Grafikart Media plugin
[https://github.com/Grafikart/CakePHP-Media][1]

I have used also dropzone.js [http://www.dropzonejs.com/][2] for multiple drag'n'drop functionality.

  [1]: https://github.com/Grafikart/CakePHP-Media
  [2]: http://www.dropzonejs.com/


#Instalation

 - Download and unrar / unzip or better
   git clone it to
   "your_app_folder/Plugin/Upload
 - Add plugin to your app by adding `CakePlugin::load('Upload', array('bootstrap' => true, 'routes' => false));` to Config/bootstrap.php
 - Edit Upload Plugin file Config/bootstrap.php
 - Add table uploads to database
 - setup Upload Plugin database connection in Config/database.php
 - make directory /webroot/files chmod 0777
 - make sure jquery.js is added to your layout

Now You can visit http://yourdomain.com/upload/uploads to see all uploads

Now You can visit http://yourdomain.com/upload/uploads/add to add new files


#PLUGIN IS STILL IN DEVELOPMENT:
#to do:

 - Add support for video, audio, pdf and whatever
 - send more ideas to cotowaty@gmail.com